//
//  StringtoTableViewApp.swift
//  StringtoTableView
//
//  Created by Abhishek Yadav on 03/06/24.
//

import SwiftUI

@main
struct StringtoTableViewApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
