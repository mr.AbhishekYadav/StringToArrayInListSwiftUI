//
//  ContentView.swift
//  StringtoTableView
//
//  Created by Abhishek Yadav on 03/06/24.
//

import SwiftUI

struct ContentView: View {
    @State private var showSplashScreen = true
    
    let originalString: String = "Abhishek Yadav iOS Developer."
    // Convert the string to an array of characters
    var characters: [String] {
        return originalString.map { String($0) }
    }
    
    var body: some View {
        Group {
            if showSplashScreen {
                SplashScreen()
                    .onAppear {
                        // Delay of 3 seconds before transitioning to the list view
                        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                            withAnimation {
                                showSplashScreen = false
                            }
                        }
                    }
            } else {
                NavigationView {
                    List(characters, id: \.self) { character in
                        Text(character)
                    }
                    .navigationTitle("List")
                }
            }
        }
    }
}
#Preview {
    ContentView()
}
